FROM debian:stable-slim

LABEL maintainer="ciol@hcor.ch"

ENV BIND_USER=bind \
    DATA_DIR=/data

COPY src/webmin-archive.asc /etc/apt/trusted.gpg.d/
COPY src/webmin.list /etc/apt/sources.list.d/

RUN rm -rf /etc/apt/apt.conf.d/docker-gzip-indexes \
 && apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y \
      bind9 bind9-host dnsutils webmin \
 && rm -rf /var/lib/apt/lists/*

COPY src/entrypoint.sh /sbin/entrypoint.sh

RUN chmod 755 /sbin/entrypoint.sh

EXPOSE 53/udp 53/tcp 10000/tcp

ENTRYPOINT ["/sbin/entrypoint.sh"]

CMD ["/usr/sbin/named"]

